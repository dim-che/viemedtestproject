The project based on the ApplicationCoordinator architecture  https://github.com/AndreyPanov/ApplicationCoordinator. 
For integration with GraphQL API used Apollo ios SDK.  
Since there was no authorization in the task, the user name and APIKey are in the code in the configuration class. 
Error handling was not finished and error from backend don't show but handling.
There are some differences from the design.
