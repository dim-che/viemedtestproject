import UIKit

final class ApplicationCoordinator: BaseCoordinator {
  
  // MARK: - Init injection
  
  private let coordinatorFactory: (CoordinatorFactoryType & CoordinatorSubFactories)
  private let window: UIWindow

  // MARK: - Life cycle
  
  init(coordinatorFactory: (CoordinatorFactoryType & CoordinatorSubFactories),
       mainWindow: UIWindow) {
    self.coordinatorFactory = coordinatorFactory
    self.window = mainWindow

    super.init()
  }

  override func start() {
    runListFlow()
  }
}

// MARK: - Private

extension ApplicationCoordinator {
  private func runListFlow() {
    let (listCoordinator, router) = coordinatorFactory.makeListCoordinator()
    addDependency(listCoordinator)
    listCoordinator.start()
    self.window.rootViewController = router.toPresent
  }
}
