import Foundation

protocol ComponentFactoryType: class {
  func makeListAPIManager() -> ListManagerType
  func makeAuthApiManager() -> AuthManagerType
}

final class ComponentFactory: ComponentFactoryType {
  func makeListAPIManager() -> ListManagerType {
    return APIManager()
  }
  
  func makeAuthApiManager() -> AuthManagerType {
    return APIManager()
  }
}
