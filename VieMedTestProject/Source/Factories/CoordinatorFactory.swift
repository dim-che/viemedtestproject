import UIKit

protocol CoordinatorSubFactories {
  var moduleFactory: ModuleFactory { get }
  var component: ComponentFactoryType { get }
}

protocol CoordinatorFactoryType {
  func makeApplicationCoordinator(window: UIWindow) -> ApplicationCoordinator
  func makeListCoordinator() -> (ListCoordinatorOutput, Presentable)
}

final class CoordinatorFactory: CoordinatorSubFactories {
  private(set) var moduleFactory: ModuleFactory
  private(set) var component: ComponentFactoryType

  init() {
    self.component = ComponentFactory()
    self.moduleFactory = ModuleFactory(component: self.component)
  }
}

extension CoordinatorFactory: CoordinatorFactoryType {
  func makeApplicationCoordinator(window: UIWindow) -> ApplicationCoordinator {
    let coordinator = ApplicationCoordinator(coordinatorFactory: self, mainWindow: window)
    return coordinator
  }

  func makeListCoordinator() -> (ListCoordinatorOutput, Presentable) {
    let router = makeRouter(nil)
    let coordinator = ListCoordinator(factory: moduleFactory, router: router)
    coordinator.listApiManager = component.makeListAPIManager()
    coordinator.authApiManager = component.makeAuthApiManager()
    return (coordinator, router)
  }
}

extension CoordinatorFactory {
  private func makeRouter(_ navigationController: UINavigationController?) -> RouterType {
    return Router(rootController: makeNavigation(navigationController))
  }

  private func makeNavigation(_ navigationController: UINavigationController?) -> UINavigationController {
    guard let navigationController = navigationController else {
      let storyboard = UIStoryboard(.main)
      return storyboard.instantiateController()
    }

    return navigationController
  }
}
