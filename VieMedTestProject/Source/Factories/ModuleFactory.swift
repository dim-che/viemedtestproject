import UIKit

protocol ListModuleFactoryType {
  func makeListOutput() -> ListOutput
  func makeDetailsOutput() -> DetailsOutput
}

final class ModuleFactory {
  let component: ComponentFactoryType

  init(component: ComponentFactoryType) {
    self.component = component
  }
}

extension ModuleFactory: ListModuleFactoryType {
  func makeListOutput() -> ListOutput {
    return UIStoryboard(.list).instantiateController() as ListController
  }
  
  func makeDetailsOutput() -> DetailsOutput {
    return UIStoryboard(.list).instantiateController() as DetailsController
  }
}
