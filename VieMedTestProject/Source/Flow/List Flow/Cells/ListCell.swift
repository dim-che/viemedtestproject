import UIKit

final class ListCell: UITableViewCell, Reusable, InterfaceBuilderPrototypable {
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var noteLabel: UILabel!
  @IBOutlet private weak var statusButton: UIButton!
  
  var onTaskStatusChanged: ((Task) -> Void)!
  var task: Task?
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    statusButton.layer.borderWidth = 2
    statusButton.layer.cornerRadius = 5
    statusButton.clipsToBounds = true
    
  }
  
  func configure(task: Task) {
    self.task = task
    titleLabel.text = task.name
    noteLabel.text = task.note
    statusButton.isSelected = task.isDone
    updateButtonColors()
  }
  
  @IBAction func changeStatusAction(_ sender: Any) {
    statusButton.isSelected = !statusButton.isSelected
    if var task = self.task {
      task.isDone = statusButton.isSelected
      updateButtonColors()
      onTaskStatusChanged(task)
    }
  }
  
  func updateButtonColors() {
    statusButton.layer.borderWidth = statusButton.isSelected ? 0 : 2
    statusButton.layer.borderColor = statusButton.isSelected ? #colorLiteral(red: 0.8470588235, green: 0.8666666667, blue: 0.8823529412, alpha: 1) : #colorLiteral(red: 0.3725490196, green: 0.4862745098, blue: 0.9333333333, alpha: 1)
    statusButton.backgroundColor = statusButton.isSelected ? #colorLiteral(red: 0.8470588235, green: 0.8666666667, blue: 0.8823529412, alpha: 1) : UIColor.white
  }
}
