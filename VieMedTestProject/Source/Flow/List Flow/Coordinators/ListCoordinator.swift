import Foundation

protocol ListCoordinatorOutput: Coordinator {
  var finishFlow: (() -> Void)? { get set }
}

final class ListCoordinator: BaseCoordinator, ListCoordinatorOutput {
  
  // MARK: - Output
  
  var finishFlow: (() -> Void)?

  // MARK: - Property injection
  
  var listApiManager: ListManagerType!
  var authApiManager: AuthManagerType!
    
  // MARK: - Init injection
  
  private let factory: ListModuleFactoryType
  private let router: RouterType

  // MARK: - Lifecycle
  
  init(factory: ListModuleFactoryType, router: RouterType) {
    self.factory = factory
    self.router = router
  }

  override func start() {
    showList()
  }
}

// MARK: - View

extension ListCoordinator {

  private func showList() {
    let listOutput = self.factory.makeListOutput()
    listOutput.onSelectItem = { [weak self] task in
      guard let self = self else { return }
      self.showDetails(item: task, completion: { item in
        listOutput.onItemAdded(item)
      }, { item in
        listOutput.onItemDeleted(item)
      })
    }
    
    listOutput.onTaskStatusChanged = { [weak self] task in
      guard let self = self else { return }
      self.updateItemStatus(task: task, completion: { item in
        listOutput.onItemUpdated(item)
      })
    }
    
    listOutput.onAddButtonPressed = {
      self.showDetails(item: nil, completion: { item in
        listOutput.onItemAdded(item)
      }, { _ in })
    }
    
    listOutput.onDeleteButtonPressed = { [weak self] taskId in
      guard let self = self else { return }
      self.deleteItem(taskId: taskId, completion: { _ in })
    }
    
    listOutput.onLoadItems = { [weak self] completion in
      guard let self = self else { return }
      guard var apiManager = self.listApiManager as? APIManagerPrivateType else { return }
      if apiManager.token != nil {
        self.getAllListItems(completion: completion)
      } else {
        self.getAccessToken(success: {[weak self] token in
            guard let self = self else { return }
            apiManager.token = token
            self.getAllListItems(completion: completion)
            }, failure: {
        })
      }
    }
    self.router.setRootModule(listOutput)
  }
  
  private func showDetails(item: Task? = nil, completion: @escaping (Task) -> Void, _ deleteCompletion: @escaping (Task) -> Void) {
    let detailsOutput = factory.makeDetailsOutput()
    detailsOutput.itemDetails = item
    detailsOutput.onDismiss = { [weak self] in
      guard let self = self else { return }
      self.router.dismissModule(animated: true, completion: nil)
    }
    
    detailsOutput.onAddItem = { [weak self] item in
      guard let self = self else { return }
      self.addItem(task: item, completion: { (item) in
        completion(item)
        self.router.dismissModule(animated: true, completion: nil)
      })
    }
    
    detailsOutput.onDeleteItem = { [weak self] task in
      guard let self = self else { return }
      if let taskId = task.id {
        self.deleteItem(taskId: taskId, completion: { result in
          if result {
            deleteCompletion(task)
          }
          self.router.dismissModule(animated: true, completion: nil)
        })
      }
    }
    router.present(detailsOutput, animated: true)
  }
}

// MARK: - API

extension ListCoordinator {
  
  // MARK: - Auth
  
  private func getAccessToken(success: @escaping (String) -> Void, failure: @escaping () -> Void) {
    let successHandler: (String) -> Void = { token in
        success(token)
    }
    let failureHandler: () -> Void = {
        failure()
    }
      authApiManager.getAccessToken(success: successHandler, failure: failureHandler)
  }
  
  // MARK: - Tasks
  
  private func getAllListItems(completion: @escaping ([Task]) -> Void) {
    let successHandler: ([Task]) -> Void = { listItems in
      completion(listItems)
    }
    let failureHandler: () -> Void = {
    }
      listApiManager.getAllListItems(success: successHandler, failure: failureHandler)
  }
  
  private func addItem(task: Task, completion: @escaping (Task) -> Void) {
    let successHandler: (Task) -> Void = { listItem in
      completion(listItem)
    }
    let failureHandler: (String) -> Void = { error in
    }
    listApiManager.createTask(task: task, success: successHandler, failure: failureHandler)
  }
  
  private func deleteItem(taskId: String, completion: @escaping (Bool) -> Void) {
    let successHandler: (Bool) -> Void = { result in
      completion(result)
    }
    let failureHandler: () -> Void = {
    }
    listApiManager.deleteTask(taskId: taskId, success: successHandler, failure: failureHandler)
  }
  
  private func updateItemStatus(task: Task, completion: @escaping (Task) -> Void) {
    let successHandler: (Task) -> Void = { listItem in
      completion(listItem)
    }
    let failureHandler: (String) -> Void = { error in
    }
    listApiManager.updateTaskStatus(task: task, success: successHandler, failure: failureHandler)
  }
}
