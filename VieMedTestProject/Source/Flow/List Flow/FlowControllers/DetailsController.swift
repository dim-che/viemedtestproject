import UIKit

protocol DetailsOutput: BaseViewType, DismissableType {
  var itemDetails: Task? { get set }
  var onAddItem: ((Task) -> Void)? { get set }
  var onDeleteItem: ((Task) -> Void)? { get set }
}

class DetailsController: BaseViewController, DetailsOutput {
  
  // MARK: - Output
  
  var onDismiss: (() -> Void)?
  var itemDetails: Task?
  var onAddItem: ((Task) -> Void)?
  var onDeleteItem: ((Task) -> Void)?
  
  // MARK: - Private
  
  @IBOutlet private weak var addButton: UIButton!
  @IBOutlet private weak var nameTextField: UITextField!
  @IBOutlet private weak var noteTextView: UITextView!
  @IBOutlet private weak var statusButton: UIButton!
  @IBOutlet private weak var deletePanelView: UIView!
  
  let placeholder = "Notes..."
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureUI()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    nameTextField.becomeFirstResponder()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    view.endEditing(true)
  }
  
  // MARK: - Actions
  
  @IBAction private func backButtonAction() {
    onDismiss?()
  }
  
  @IBAction func addButtonAction(_ sender: Any) {
    
    if let task = self.itemDetails {
      onAddItem?(task)
    } else {
      let taskName = nameTextField.text ?? ""
      var newTask = Task(name: taskName)
      if !noteTextView.text.isEmpty && noteTextView.text != placeholder {
        newTask.note = noteTextView.text
      }
      onAddItem?(newTask)
    }
  }
  
  @IBAction func deleteButtonAction(_ sender: Any) {
    if let task = self.itemDetails {
      onDeleteItem?(task)
    }
  }

  // MARK: - Private
  
  fileprivate func configureUI() {
    
    nameTextField.delegate = self
    noteTextView.delegate = self
    noteTextView.text = placeholder
    noteTextView.textColor = UIColor.lightGray
    
    statusButton.layer.borderWidth = 2
    statusButton.layer.cornerRadius = 5
    statusButton.clipsToBounds = true
    
    addButton.isHidden = itemDetails != nil
    deletePanelView.isHidden = !addButton.isHidden
    nameTextField.isEnabled = !addButton.isHidden
    
    if let task = self.itemDetails {
      nameTextField.text = task.name
      noteTextView.text = task.note
      statusButton.isSelected = task.isDone
    }
    updateButtonColors()
    chackAddButtonStatus()
  }
  
  fileprivate func updateButtonColors() {
    statusButton.layer.borderWidth = statusButton.isSelected ? 0 : 2
    statusButton.layer.borderColor = statusButton.isSelected ? #colorLiteral(red: 0.8470588235, green: 0.8666666667, blue: 0.8823529412, alpha: 1) : #colorLiteral(red: 0.3725490196, green: 0.4862745098, blue: 0.9333333333, alpha: 1)
    statusButton.backgroundColor = statusButton.isSelected ? #colorLiteral(red: 0.8470588235, green: 0.8666666667, blue: 0.8823529412, alpha: 1) : UIColor.white
  }
  
  fileprivate func chackAddButtonStatus() {
    addButton.isEnabled = !(nameTextField.text ?? "").isEmpty
  }
}

extension DetailsController: UITextFieldDelegate {
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
      addButton.isEnabled = !updatedString.isEmpty
    } else {
      addButton.isEnabled = false
    }
    return true
  }

  func textFieldDidBeginEditing(_ textField: UITextField) {
    chackAddButtonStatus()
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    noteTextView.becomeFirstResponder()
    return true
  }
}

extension DetailsController: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.text == placeholder {
      textView.text = ""
      textView.textColor = UIColor.black
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text == "" {
      textView.text = placeholder
      textView.textColor = UIColor.lightGray
    }
  }

}
