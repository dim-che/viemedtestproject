import UIKit
import DZNEmptyDataSet

protocol ListOutput: BaseViewType {
  var onLoadItems: ((@escaping ([Task]) -> Void) -> Void)? { get set }
  var onSelectItem: ((Task) -> Void)? { get set }
  var listItems: [Task] { get set }
  var onAddButtonPressed: (() -> Void)? { get set }
  func onItemAdded(_ item: Task)
  var onDeleteButtonPressed: ((String) -> Void)? { get set }
  var onTaskStatusChanged: ((Task) -> Void)? { get set }
  func onItemDeleted(_ item: Task)
  func onItemUpdated(_ item: Task)
}

class ListController: BaseViewController, ListOutput, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
  
  let doneSectionTitle = "ALREADY DONE"
  let todaySectionTitle = "My Tasks for today".uppercased()
  
  // MARK: - Output
  
  var onSelectItem: ((Task) -> Void)?
  var onLoadItems: ((@escaping ([Task]) -> Void) -> Void)?
  var listItems = [Task]()
  var onAddButtonPressed: (() -> Void)?
  var onDeleteButtonPressed: ((String) -> Void)?
  var onTaskStatusChanged: ((Task) -> Void)?
  
  // MARK: - Private
  
  @IBOutlet private weak var tableView: UITableView!
  
  private var todayTasks = [Task]()
  private var doneTasks = [Task]()
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureTableView()
    loadItems()
  }

  // MARK: - Private
  fileprivate func configureTableView() {
    tableView.register(ListCell.self)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.estimatedRowHeight = 48
    tableView.rowHeight = UITableView.automaticDimension
    tableView.emptyDataSetSource = self
    tableView.emptyDataSetDelegate = self
    tableView.tableFooterView = UIView()
  }
    
  fileprivate func loadItems() {
    onLoadItems? { [weak self] listItems in
      guard let self = self else { return }
      self.listItems = listItems
      self.updateDataSource()
    }
   }
  
  fileprivate func updateDataSource() {
    reloadDataSourceArrays()
    tableView.reloadData()
  }
  
  fileprivate func reloadDataSourceArrays() {
    todayTasks = listItems.filter { $0.isDone == false }
    doneTasks = listItems.filter { $0.isDone == true }
  }
  
  // MARK: - Actions
  
  @IBAction func addButtonTouch(_ sender: Any) {
    onAddButtonPressed?()
  }
  
  func onItemAdded(_ item: Task) {
    listItems.append(item)
    updateDataSource()
  }
  
  func onItemDeleted(_ item: Task) {
    if let index = self.listItems.firstIndex(where: { $0.id == item.id }) {
      self.listItems.remove(at: index)
      updateDataSource()
    }
  }
  
  func onItemUpdated(_ item: Task) {
    if let index = self.listItems.firstIndex(where: { $0.id == item.id }) {
      self.listItems[index].isDone = item.isDone
      updateDataSource()
    }
  }
}

// MARK: - Table view

extension ListController: UITableViewDelegate, UITableViewDataSource {
  
  func dataSouce(for section: Int) -> [Task] {
    switch section {
    case 0:
      return todayTasks
    case 1:
      return doneTasks
    default:
      return [Task]()
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeue(ListCell.self, for: indexPath)
    if indexPath.row < dataSouce(for: indexPath.section).count {
      cell.configure(task: dataSouce(for: indexPath.section)[indexPath.row])
      cell.onTaskStatusChanged = { [weak self] task in
        guard let self = self else { return }
        self.onTaskStatusChanged?(task)
      }
    }
    return cell
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSouce(for: section).count
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row < dataSouce(for: indexPath.section).count {
      onSelectItem?(dataSouce(for: indexPath.section)[indexPath.row])
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return dataSouce(for: section).isEmpty ? 0 : 45
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return section == 0 ? todaySectionTitle : doneSectionTitle
  }
  
  func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    let header = view as! UITableViewHeaderFooterView
    header.textLabel?.font = UIFont.boldSystemFont(ofSize: 12)
    header.textLabel?.textColor = UIColor.lightGray
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let delete = UITableViewRowAction(style: .destructive, title: "Delete") { [weak self] (_, indexPath) in
      guard let self = self else { return }
      if indexPath.row < self.listItems.count, let taskId = self.listItems[indexPath.row].id {
        if let index = self.listItems.firstIndex(where: { $0.id == taskId }) {
          self.listItems.remove(at: index)
        }
        self.updateDataSource()
        self.onDeleteButtonPressed?(taskId)
      }
    }
    return [delete]
  }
  
  // MARK: - Deal with the empty data set
  
  func description(forEmptyDataSet _: UIScrollView!) -> NSAttributedString! {
    let str = "There are no tasks\nfor today"
    let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
    return NSAttributedString(string: str, attributes: attrs)
  }
  
  func image(forEmptyDataSet _: UIScrollView!) -> UIImage! {
    return UIImage(named: "EmptyListIcon")
  }
}
