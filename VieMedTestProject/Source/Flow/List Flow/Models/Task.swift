import Foundation

struct Task {
  let id: String?
  let name: String
  var note: String?
  var isDone: Bool
  
  init(name: String, note: String? = nil, isDone: Bool = false) {
    self.id = nil
    self.name = name
    self.note = note
    self.isDone = isDone
  }
  
  init(graphTask: CreateTaskMutation.Data.CreateTask) {
    self.id = graphTask.id
    self.name = graphTask.name
    self.note = graphTask.note
    self.isDone = graphTask.isDone
  }
  
  init(allTask: AllTasksQuery.Data.AllTask) {
    self.id = allTask.id
    self.name = allTask.name
    self.note = allTask.note
    self.isDone = allTask.isDone
  }
  
  init(updateTaskStatus: UpdateTaskStatusMutation.Data.UpdateTaskStatus) {
    self.id = updateTaskStatus.id
    self.name = updateTaskStatus.name
    self.note = updateTaskStatus.note
    self.isDone = updateTaskStatus.isDone
  }
  
}
