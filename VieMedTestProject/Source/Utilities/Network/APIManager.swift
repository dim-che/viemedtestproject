import Foundation
import Apollo

struct ServerConfig {
    static let baseUrl = "https://380odjc5vi.execute-api.us-east-1.amazonaws.com/dev/graphql"
    static let apiKey = "1a2a3f14-49c5-4bf9-a391-2510a0edc1e8"
    static let userName = "Dim"
}

protocol APIManagerPrivateType {
    func perform<Mutation: GraphQLMutation>(mutation: Mutation, resultHandler: OperationResultHandler<Mutation>?)
    var token: String? { get set }
}

final class APIManager: APIManagerPrivateType {
    var token: String?
    
    private var apollo: ApolloClient {
        let configuration = URLSessionConfiguration.default
        if let token = self.token {
            configuration.httpAdditionalHeaders = ["Authorization": "Bearer \(token)"]
        }
        guard let url = URL(string: ServerConfig.baseUrl) else { fatalError() }
        
        return ApolloClient(networkTransport: HTTPNetworkTransport(url: url, configuration: configuration))
    }
    
    func perform<Mutation: GraphQLMutation>(mutation: Mutation,
                                            resultHandler: OperationResultHandler<Mutation>?) {
        apollo.perform(mutation: mutation, queue: .main, resultHandler: resultHandler)
    }
    
    func select<Query: GraphQLQuery>(query: Query,
                                     resultHandler: OperationResultHandler<Query>?) {
      apollo.fetch(query: query, queue: .main, resultHandler: resultHandler)
    }
}
