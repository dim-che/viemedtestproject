import Foundation

protocol AuthManagerType {
    func getAccessToken(success: @escaping (String) -> Void, failure: @escaping () -> Void)
}

extension APIManager: AuthManagerType {
    func getAccessToken(success: @escaping (String) -> Void, failure: @escaping () -> Void) {
      let authMutation = GenerateAccessTokenMutation(apiKey: ServerConfig.apiKey, userName: ServerConfig.userName)
      perform(mutation: authMutation) { (result, error) in
        if let result = result {
          if let token = result.data?.generateAccessToken {
            success(token)
          } else {
            failure()
          }
        } else {
            print(error as Any)
            failure()
        }
      }
    }
}
