import Foundation

protocol ListManagerType {
  func getAllListItems(success: @escaping ([Task]) -> Void, failure: @escaping () -> Void?)
  func createTask(task: Task, success: @escaping (Task) -> Void, failure: @escaping (String) -> Void?)
  func deleteTask(taskId: String, success: @escaping (Bool) -> Void, failure: @escaping () -> Void?)
  func updateTaskStatus(task: Task, success: @escaping (Task) -> Void, failure: @escaping (String) -> Void?)
}

extension APIManager: ListManagerType {

  func getAllListItems(success: @escaping ([Task]) -> Void, failure: @escaping () -> Void?) {
    let allTask = AllTasksQuery()
    select(query: allTask) { (result, error) in
      if let result = result {
        if let graphTasks = result.data?.allTasks {
          var tasks = [Task]()
          graphTasks.forEach({ (graphTask) in
            if let allTask = graphTask {
              tasks.append(Task(allTask: allTask))
            }
          })
          success(tasks)
        } else {
          failure()
        }
      } else {
        print(error.debugDescription)
        failure()
      }
    }
  }
  
  func createTask(task: Task, success: @escaping (Task) -> Void, failure: @escaping (String) -> Void?) {
    let createTaskMutation = CreateTaskMutation(name: task.name, note: task.note, isDone: task.isDone)
    perform(mutation: createTaskMutation) { (result, error) in
      if let result = result {
        if let graphTask = result.data?.createTask {
          let task = Task(graphTask: graphTask)
          success(task)
        } else {
          failure("An error occurred while creating the task")
        }
      } else {
        failure(error?.localizedDescription ?? "An error occurred while creating the task")
      }
    }
  }
  
  func deleteTask(taskId: String, success: @escaping (Bool) -> Void, failure: @escaping () -> Void?) {
    let deleteTaskMutation = DeleteTaskMutation(id: taskId)
    perform(mutation: deleteTaskMutation) { (result, error) in
      if let result = result {
        if let deleteTaskResult = result.data?.deleteTask {
          success(deleteTaskResult)
        } else {
          failure()
        }
      }else {
        print(error.debugDescription)
        failure()
      }
    }
  }
  
  func updateTaskStatus(task: Task, success: @escaping (Task) -> Void, failure: @escaping (String) -> Void?) {
    let updateTaskMutation = UpdateTaskStatusMutation(id: task.id ?? "", isDone: task.isDone)
    perform(mutation: updateTaskMutation) { (result, error) in
      if let result = result {
        if let graphTask = result.data?.updateTaskStatus {
          let task = Task(updateTaskStatus: graphTask)
          success(task)
        } else {
          failure("An error occurred while updating the task")
        }
      } else {
        failure(error?.localizedDescription ?? "An error occurred while updating the task")
      }
    }
  }
}
